const test = require('tape')
const axios = require('axios')

const P2PTF = require('../dist/index')

const announceURLs = [
  'ws://localhost:5000'
  // 'wss://tracker.btorrent.xyz:443/announce'
]

test('file send', t => {
  const p2ptf1 = new P2PTF({
    announce: announceURLs
  })

  const p2ptf2 = new P2PTF({
    announce: announceURLs
  })

  // get a file
  axios.get('/test/cat.jpg', {
    responseType: 'blob'
  }).then(response => {
    const file = new File(response.data)
    p2ptf1.seed(file).then(torrent => {
      console.log(torrent)

      p2ptf2.add(torrent.infoHash)
    })
  })
})
