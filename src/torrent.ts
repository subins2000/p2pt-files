const P2PT = require('p2pt')

interface options {
  name: string,
  announce: [],
  tracker: boolean
}

class Torrent {
  private opts: options;
  
  public infoHash: string;

  constructor (name: string, files: [File | Buffer], opts: {}) {
    this.opts = {
      ...opts,
      ...{
        // default options
        name: 'name' in files[0] ? files[0].name : '',
        announce: [],
        tracker: true
      }
    }

    const length = files.reduce((sum: number, file: File | Buffer): number => {
      if ('length' in file) {
        return sum + file.length
      } else {
        return sum + file.size
      }
    }, 0)

    // xxhash gives string of length 7
    // Make 6 of them and subtract to pretend it's a sha1
    // const pieceOffset = Math.floor(length / 6)
    this.infoHash = '6f9b9af3cd6e8b8a73c2cdced37fe9f59226e27d'

    this.startP2PT()
  }

  startP2PT() {
    const p2pt = new P2PT(this.opts.announce)
    p2pt.setIdentifier(this.infoHash)

    p2pt.on('peerconnect', peer => {
      p2pt.send(peer, {
        name: this.opts.name,
        length
      })
    })

    if (this.opts.tracker) {
      p2pt.start()
    }
  }

  addPeer (peer) {
  }
}
