/**
 * P2PT-Files
 * Simple, fast file transfer over WebRTC using WebTorrent Trackers
 * Copyright Subin Siby <mail@subinsb.com>, 2020
 * LGPL licensed
 */

import { builtinModules } from "module"

const EventEmitter = require('events')
const XXH = require('xxhashjs')

const Torrent = require('./torrent')

class P2PTF extends EventEmitter {
  constructor (opts: {}) {
    super()

    this.opts = {
      ...opts,
      ...{
        // default options
        announce: [],
        tracker: true
      }
    }

    this.files = {}
  }

  // Add a file to download
  add (infoHash, opts, callback) {
  }

  /**
   * Start seeding files
   * input can be File, FileList, Buffer (node)
   */
  seed (input, opts) {
    return new Promise((resolve, reject) => {
      if (input instanceof File) {
        input = [input]
      }

      new Torrent()

      resolve()
    })
  }

  /// do xxhash
  xxh (input) {
    return XXH.h32(input, 0xABCDEF).toString(16)
  }
}

module.exports = P2PTF
